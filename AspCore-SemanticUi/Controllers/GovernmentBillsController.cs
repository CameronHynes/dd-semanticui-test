using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DigitalDemocracy.DataAccess.Interfaces;
using Newtonsoft.Json;

namespace AspCore_SemanticUi.Controllers
{
    public class GovernmentBillsController : Controller
    {
        public IActionResult Index()
        {
			string billsJson = System.IO.File.ReadAllText(@"./governmentBills.json");
			//return Ok(JsonConvert.DeserializeObject<GovernmentBill[]>(billsJson));
			return View(JsonConvert.DeserializeObject<GovernmentBill[]>(billsJson));
		}
    }
}