﻿namespace DigitalDemocracy.DataAccess.Interfaces
{
    public class GovernmentParty
    {
        public int Id { get; set; }

        public string PartyName { get; set; }

        public string ShortPartyName { get; set; }

        public string PrimaryColor { get; set; }

        public string Shading { get; set; }

        public string Description { get; set; }

        public string DescriptionUrl { get; set; }

        public string FoundedYear { get; set; }

        public string PoliciesUrl { get; set; }

        public string PartyLogoUrl { get; set; }
    }
}
