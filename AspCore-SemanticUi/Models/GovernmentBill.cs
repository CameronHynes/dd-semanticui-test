﻿using System.Collections.Generic;

namespace DigitalDemocracy.DataAccess.Interfaces
{
    public class GovernmentBill
    {
        /// <summary>
        /// Gets or sets the unique identifier of this bill for internal purposes only.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name of this <see cref="GovernmentBill"/>.
        /// This is the public facing name of this bill.
        /// </summary>        
        public string Name { get; set; }

        public string SluggedBillName { get; set; }

        public ICollection<BillReading> Readings { get; set; } = new List<BillReading>();
    }
}
