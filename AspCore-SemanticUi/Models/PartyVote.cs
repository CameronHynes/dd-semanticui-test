﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalDemocracy.DataAccess.Interfaces
{
    public class PartyVote
    {
		public int Id { get; set; }
		public int Ayes { get; set; }
		public int Noes { get; set; }

		public virtual GovernmentParty Party { get; set; }
		public virtual GovernmentBill GovernmentBill { get; set; }
	}
}
