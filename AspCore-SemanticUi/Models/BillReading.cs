﻿using System;
using System.Collections.Generic;

namespace DigitalDemocracy.DataAccess.Interfaces
{
    public class BillReading
    {
        /// <summary>
        /// Gets or sets the reading stage that this <see cref="GovernmentBill"/> is currently at.
        /// A bill goes through several reading stages before it becomes law.
        /// </summary>
        public string ReadingStage { get; set; }

        /// <summary>
        /// Gets a value indicating whether this bill was passed into law.
        /// </summary>
        public bool IsBillPassed { get; set; }

        /// <summary>
        /// Gets or sets the date that this bill will be voted on.
        /// If <see cref="ReadingStage"/> is set to <see cref="BillReadingStage.Passed"/> then this will be a past date.
        /// If this is null then a date has not currently been set.
        /// </summary>
        public DateTime? VoteDate { get; set; }

        /// <summary>
        /// Gets or sets the number of Yes votes for this bill, as in, the of number votes which were in favour of this bill.
        /// "Ayes" is an old fashioned term for "Yes"/"Agree"/"In Favour".
        /// See <see cref="Noes"/> for the number of votes against this bill.
        /// </summary>
        public int? Ayes { get; set; }

        /// <summary>
        /// Gets or sets the number of No votes for this bill, as in, the number of votes where were against this bill.
        /// See <see cref="Ayes"/> for the number of votes in favour of this bill.
        /// </summary>
        public int? Noes { get; set; }

        /// <summary>
        /// Gets or sets the difference between <see cref="Ayes"/> and <see cref="Noes"/>.
        /// </summary>
        public int? Majority { get; set; }

        /// <summary>
        /// Gets or sets the number of parliament members that were absent for the vote of this bill.
        /// </summary>
        public int? Abstentions { get; set; }

        /// <summary>
        /// Gets or sets additional notes associated with this bill.
        /// </summary>
        public string Notes { get; set; }

		public List<PartyVote> PartyVotes { get; set; }
    }
}
